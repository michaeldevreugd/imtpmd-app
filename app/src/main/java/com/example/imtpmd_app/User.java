package com.example.imtpmd_app;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class User {
    @PrimaryKey
    @NonNull
    private String email;
    private String username;
    private int xp;
    @ColumnInfo(name = "completed")
    private String completed;
    @ColumnInfo(name = "active")
    private String active;
    @ColumnInfo(name = "uncompleted")
    private String uncompleted;

    public User(){}

    //getters
    public String getEmail(){return this.email;}
    public String getUsername(){return this.username;}


    public int getXp(){return this.xp;}
    public String getCompleted(){return this.completed;}
    public String getActive(){return this.active;}
    public String getUncompleted(){return this.uncompleted;}

    //setters

    public void setEmail(String email){this.email = email;}
    public void setUsername(String username){this.username = username;}

    public void setXp(int xp){this.xp = xp;}
    public void setCompleted(String achievements){this.completed = achievements;}
    public void setActive(String achievements){this.active = achievements;}
    public void setUncompleted(String achievements){this.uncompleted = achievements;}
}

