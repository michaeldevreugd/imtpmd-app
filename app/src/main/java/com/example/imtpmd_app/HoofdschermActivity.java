package com.example.imtpmd_app;

import android.app.Dialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HoofdschermActivity extends AppCompatActivity {

    private static final String TAG = "HoofdschermActivity";
    private LevelSystem levelSystem;
    private int totalXp;
    private String username;
    private User u;

    private Dialog dialog;
    private TextView titlePopup;
    private ImageView imagePopup;
    private TextView textPopup;
    private Button buttonClosePopup;

    private TextView usernameIndicator;
    private TextView xpIndicator;
    private TextView levelIndicator;
    private ProgressBar progressBar;
    private SharedPreferences pref;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hoofdscherm);

        pref = getApplicationContext().getSharedPreferences("AppPref", MODE_PRIVATE);
        final ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        final AppDatabase db = Room
                .databaseBuilder(getApplicationContext(), AppDatabase.class, "database")
                .build();

        final ApiService service = RetrofitBuilder.createService(ApiService.class);

        usernameIndicator = findViewById(R.id.username_indicator);
        xpIndicator = findViewById(R.id.xp_indicator);
        levelIndicator = findViewById(R.id.level_indicator);
        progressBar = findViewById(R.id.progressBar);

        Button achievementButton = (Button) findViewById(R.id.achievementButton);
        Button helpButton = (Button) findViewById(R.id.helpButton);

        dialog = new Dialog(this);

        final Thread getUserDetails = new Thread(){
            @Override
            public void run(){
                u = db.userDao().loadUserByEmail(pref.getString("loggedInUser", ""));
                totalXp = u.getXp();
                username = u.getUsername();
                levelSystem = new LevelSystem(totalXp);
                setViewData();
                db.close();
                return;
            }
        };

        //checken of device verbonden is met internet
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
        {
            //device is verbonden met internet
            Call<UserInformation> getUserInformationCall = service.getuserdata( "Bearer " + pref.getString("bearer", ""));
            getUserInformationCall.enqueue(new Callback<UserInformation>() {
                @Override
                public void onResponse(Call<UserInformation>call, final Response<UserInformation> response) {
                    Thread syncUserData = new Thread(){
                        @Override
                        public void run(){
                            u = db.userDao().loadUserByEmail(pref.getString("loggedInUser", ""));
                            if(u.getXp() != response.body().xp || !u.getUncompleted().matches(response.body().uncompleted) || !u.getActive().matches(response.body().active) ||
                                    !u.getCompleted().matches(response.body().completed)){
                                // data is niet in sync
                                // hier wordt lokale data verstuurd naar de api om deze weer te synchroniseren
                                UserStatistics userStatistics = new UserStatistics(u.getActive(), u.getCompleted(), u.getUncompleted(), u.getXp());
                                Call<UserStatistics> updateUserInformationCall = service.userdata(userStatistics, "Bearer " + pref.getString("bearer", ""));
                                updateUserInformationCall.enqueue(new Callback<UserStatistics>() {
                                    @Override
                                    public void onResponse(Call<UserStatistics>call, Response<UserStatistics> response) {
                                        Log.d("sync", "Data is synced");
                                        totalXp = u.getXp();
                                        username = u.getUsername();
                                        levelSystem = new LevelSystem(totalXp);
                                        setViewData();
                                    }

                                    @Override
                                    public void onFailure(Call<UserStatistics> call, Throwable t) {
                                        Log.d("fout", t.toString());
                                    }
                                });
                            } else{
                                totalXp = response.body().xp;
                                username = response.body().name;
                                levelSystem = new LevelSystem(totalXp);
                                setViewData();
                            }
                            db.close();
                            return;
                        }
                    };
                    syncUserData.start();
                }

                @Override
                public void onFailure(Call<UserInformation> call, Throwable t) {
                    getUserDetails.start();
                }
            });
        }else{
            // device is niet verbonden met internet
            getUserDetails.start();
        }


        achievementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent achievementScherm = new Intent(getApplicationContext(), AchievementSchermActivity.class);
                startActivity(achievementScherm);
                finish();
            }
        });

        helpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent helpscherm = new Intent(getApplicationContext(), HelpSchermActivity.class);
                startActivity(helpscherm);
                finish();
            }
        });
    }

    // Hier worden de juiste waarden aan de views gekoppeld
    public void setViewData(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                popupSysteem();

                usernameIndicator.setText(username);
                xpIndicator.setText("XP " + levelSystem.getLevelXp() + " / " + levelSystem.getCurrentLevelXpCap());
                levelIndicator.setText("Level " + levelSystem.getLevel());
                progressBar.setProgress(levelSystem.getLevelXp());
            }
        });
    }

    public void popupSysteem()
    {
        dialog.setContentView(R.layout.popupscherm_level_hoofdscherm);
        buttonClosePopup = dialog.findViewById(R.id.closePopup);
        titlePopup = dialog.findViewById(R.id.titlePopup);
        imagePopup = dialog.findViewById(R.id.imagePopup);
        textPopup = dialog.findViewById(R.id.textPopup);


        final SharedPreferences.Editor popupEditor = pref.edit();

        //Hier kijken of een popup getriggered moet worden.
        if(levelSystem.getLevel() >= 2 && levelSystem.getLevel() <5 && pref.getBoolean("level2",true))
        {
            Log.d("LevelSysteem","Gefeliciteerd, u heeft level 2 behaald");
            ShowPopup();
            titlePopup.setText("Noob");
            imagePopup.setImageResource(R.drawable.popupnummereen);
            textPopup.setText("Je bent goed bezig. Blijf jezelf uitdagen");
            popupEditor.putBoolean("level2",false);
            popupEditor.commit();
        }

        if(levelSystem.getLevel() >= 5 && levelSystem.getLevel() <10 && pref.getBoolean("level5", true))
        {
            Log.d("LevelSysteem","Gefeliciteerd, u heeft level 5 behaald");
            ShowPopup();
            titlePopup.setText("Pro");
            imagePopup.setImageResource(R.drawable.popupnummertwee);
            textPopup.setText("Je bent comfortabeler met jezelf. Hou dit vol!");
            popupEditor.putBoolean("level5",false);
            popupEditor.commit();
        }

        if(levelSystem.getLevel() == 10 && levelSystem.getLevelXp() == 55 && pref.getBoolean("level10", true))
        {
            Log.d("LevelSysteem","Gefeliciteerd, u heeft level 10 behaald");
            ShowPopup();
            titlePopup.setText("Veteran");
            imagePopup.setImageResource(R.drawable.popupnummerdrie);
            textPopup.setText("Je hebt het maximale level behaald.");
            popupEditor.putBoolean("level10",false);
            popupEditor.commit();
        }
    }


    public void ShowPopup()
    {
        buttonClosePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}
