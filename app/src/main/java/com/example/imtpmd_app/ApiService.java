package com.example.imtpmd_app;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiService {

    @POST("register")
    @FormUrlEncoded
    Call<AccessToken> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password);

    @POST("login")
    @FormUrlEncoded
    Call<AccessToken> login(@Field("username") String username, @Field("password") String password);

    @POST("updateuserdata")
    Call<UserStatistics> userdata(@Body UserStatistics userStatistics, @Header("Authorization") String bearerToken);

    @GET("getuserdata")
    Call<UserInformation> getuserdata(@Header("Authorization") String bearerToken);
}
