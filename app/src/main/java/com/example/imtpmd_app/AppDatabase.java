package com.example.imtpmd_app;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {User.class}, version=8)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}
