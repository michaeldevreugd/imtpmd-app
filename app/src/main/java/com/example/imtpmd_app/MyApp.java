package com.example.imtpmd_app;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;

public class MyApp extends Application {
    @Override
    public void onCreate()
    {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }

}
