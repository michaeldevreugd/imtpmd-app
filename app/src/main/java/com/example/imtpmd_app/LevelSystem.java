package com.example.imtpmd_app;

import android.util.Log;

public class LevelSystem {

    private int totalXp;
    private int level;
    private int levelXp;
    private int currentLevelXpCap = 55;

    public LevelSystem(int totalXp){
        this.totalXp = totalXp;
        if(this.totalXp < 55){
            this.level = 1;
            this.levelXp = totalXp;
        } else if(this.totalXp >= 55 && this.totalXp < 110){
            this.level = 2;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 110 && this.totalXp < 165){
            this.level = 3;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 165 && this.totalXp < 220){
            this.level = 4;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 220 && this.totalXp < 275){
            this.level = 5;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 275 && this.totalXp < 330){
            this.level = 6;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 330 && this.totalXp < 385){
            this.level = 7;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 385 && this.totalXp < 440){
            this.level = 8;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 440 && this.totalXp < 495){
            this.level = 9;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        } else if(this.totalXp >= 495 && this.totalXp <= 550){
            this.level = 10;
            this.levelXp = this.totalXp - ((this.level - 1) * 55);
        }
    }

    public int getLevel() {
        return level;
    }

    public int getLevelXp() {
        return levelXp;
    }

    public int getTotalXp() {
        return totalXp;
    }

    public int getCurrentLevelXpCap() {
        return currentLevelXpCap;
    }
}
