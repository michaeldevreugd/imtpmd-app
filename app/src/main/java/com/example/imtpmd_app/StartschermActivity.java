package com.example.imtpmd_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class StartschermActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startscherm);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("AppPref", MODE_PRIVATE);

        Thread thread1 = new Thread(){
          @Override
          public void run(){
              try {
                  sleep(2000);
                  if(pref.getBoolean("registered", false)){
                      Intent loginScherm = new Intent(getApplicationContext(), LoginActivity.class);
                      startActivity(loginScherm);
                  } else {
                      Intent registerscherm = new Intent(getApplicationContext(), OnboardingActivity.class);
                      startActivity(registerscherm);
                  }
                  finish();
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }

          }
        };
        thread1.start();
    }
}
