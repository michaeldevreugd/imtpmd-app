package com.example.imtpmd_app;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AchievementAdapter extends RecyclerView.Adapter<AchievementAdapter.AchievementViewHolder> {

    private ArrayList<Achievement> achievementList;
    private String layout;
    private OnItemClickListener listener;

    public interface OnItemClickListener{
        void onStartClick(int position);
        void onCancelClick(int position);
        void onFinishClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    public static class AchievementViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public TextView title;
        public TextView description;
        public TextView xp;
        public Button startAchievement;
        public Button cancelAchievement;
        public Button finishAchievement;

        public AchievementViewHolder(@NonNull View itemView, final OnItemClickListener listener, String layout) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.achievementImage);
            this.title = itemView.findViewById(R.id.achievementTitle);
            this.description = itemView.findViewById(R.id.achievementDesc);
            this.xp = itemView.findViewById(R.id.AchievementXp);
            this.startAchievement = itemView.findViewById(R.id.startAchievementButton);
            this.cancelAchievement = itemView.findViewById(R.id.cancelActiveAchievementButton);
            this.finishAchievement = itemView.findViewById(R.id.finishActiveAchievementButton);

            switch(layout){
                case "UNCOMPLETED":
                    startAchievement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(listener != null) {
                                int position = getAdapterPosition();
                                if(position != RecyclerView.NO_POSITION){
                                    listener.onStartClick(position);
                                }
                            }
                        }
                    });
                    break;
                case "ACTIVE":
                    cancelAchievement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(listener != null) {
                                int position = getAdapterPosition();
                                if(position != RecyclerView.NO_POSITION){
                                    listener.onCancelClick(position);
                                }
                            }
                        }
                    });

                    finishAchievement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(listener != null) {
                                int position = getAdapterPosition();
                                if(position != RecyclerView.NO_POSITION){
                                    listener.onFinishClick(position);
                                }
                            }
                        }
                    });
                    break;
            }
        }
    }

    public AchievementAdapter(ArrayList<Achievement> achievementList, String layout){
        this.achievementList = achievementList;
        this.layout = layout;
    }

    @NonNull
    @Override
    public AchievementViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = null;
        switch(this.layout){
            case "UNCOMPLETED":
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.uncompleted_achievementlist_item, viewGroup, false);
                break;
            case "COMPLETED":
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.completed_achievementlist_item, viewGroup, false);
                break;
            case "ACTIVE":
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.active_achievementlist_item, viewGroup, false);
                break;
        }
        AchievementViewHolder avh = new AchievementViewHolder(v, listener, this.layout);
        return avh;
    }

    @Override
    public void onBindViewHolder(@NonNull AchievementViewHolder achievementViewHolder, int i) {
        Achievement currentAchievement = this.achievementList.get(i);

        achievementViewHolder.imageView.setImageResource(currentAchievement.getImageResource());
        achievementViewHolder.title.setText(String.valueOf(currentAchievement.getTitle()));
        achievementViewHolder.description.setText(String.valueOf(currentAchievement.getDescription()));
        achievementViewHolder.xp.setText(Integer.toString(currentAchievement.getXp()) + " xp");
    }

    @Override
    public int getItemCount() {
        return this.achievementList.size();
    }
}
