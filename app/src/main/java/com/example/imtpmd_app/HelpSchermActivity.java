package com.example.imtpmd_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class HelpSchermActivity extends AppCompatActivity {

    private ArrayList<String> messages = new ArrayList();
    private int currentMessage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_scherm);

        final TextView indicator = findViewById(R.id.helpSchermIndicator);
        final TextView text = findViewById(R.id.helpSchermText);
        Button left = (Button)findViewById(R.id.helpSchermButtonLeft);
        Button right = (Button)findViewById(R.id.helpSchermButtonRight);
        Button backButton = findViewById(R.id.helpSchermBackButton);

        messages.add("Your limitation—it’s only your imagination.");
        messages.add("Social anxiety often stems from a belief that you are not “enough” in some way: the sensation of not being enough often comes from the anxiety that disrupts your sense of self.  This anxiety fragments your self-concept.  The reality is that “enough” does not exist and that you are likely at least as “enough” as anybody else.");
        messages.add("Push yourself, because no one else is going to do it for you.");
        messages.add("Great things never come from comfort zones.");
        messages.add("Sometimes later becomes never. Do it now.");
        messages.add("Do not expect that the fears will go away immediately.  Recognize that many of them are unconscious and will take time to “find”.");
        messages.add("Overcoming your fears is definitely possible and has been achieved by many people.  But this is not an overnight process. Many other ancillary techniques such as meditation and exercise can be helpful.");
        messages.add("The harder you work for something, the greater you’ll feel when you achieve it.");
        messages.add("Bring a cheat sheet. Before going into an anxiety-inducing situation, anticipate what anxious thoughts you’ll have and challenge them on a piece of paper. Bring this piece of paper with you to the event (or save it on your phone). Then if you start feeling nervous, you can look at it to remind yourself of your thought challenges and calm yourself down.");
        messages.add("Wake up with determination. Go to bed with satisfaction.");

        text.setText(messages.get((currentMessage -1)));
        text.setMovementMethod(new ScrollingMovementMethod());
        indicator.setText(Integer.toString(currentMessage) + " / " + Integer.toString(messages.size()));

        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("test", "test");
                if (currentMessage != 1){
                    currentMessage--;
                    indicator.setText(Integer.toString(currentMessage) + " / " + Integer.toString(messages.size()));
                    text.setText(messages.get((currentMessage -1)));
                    text.setScrollY(0);
                }
            }
        });

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "dit is een test", Toast.LENGTH_SHORT);
                if (currentMessage < messages.size()){
                    Log.d("test", "test");
                    currentMessage++;
                    indicator.setText(Integer.toString(currentMessage) + " / " + Integer.toString(messages.size()));
                    text.setText(messages.get((currentMessage -1)));
                    text.setScrollY(0);
                }
            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hoofdscherm = new Intent(getApplicationContext(), HoofdschermActivity.class);
                startActivity(hoofdscherm);
                finish();
            }
        });
    }
}
