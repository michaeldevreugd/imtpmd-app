package com.example.imtpmd_app;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AchievementSchermActivity extends AppCompatActivity {

    private Thread setUserData;
    private AppDatabase db;

    // lijsten van achievements die worden opgebouwd met id's die uit backend ontvangen worden
    private ArrayList<Achievement> allAchievements;
    private ArrayList<Achievement> uncompletedAchievements;
    private ArrayList<Achievement> activeAchievements;
    private ArrayList<Achievement> completedAchievements;

    // id's die worden ontvangen uit backend
    private String uncompletedIds;
    private String activeIds;
    private String completedIds;

    // id's die verstuurt worden naar backend
    private String currentUncompletedIds;
    private String currentActiveIds;
    private String currentCompletedIds;
    private int xp;

    private RecyclerView uncompletedList;
    private RecyclerView activeList;
    private RecyclerView completedList;
    private AchievementAdapter uncompletedListAdapter;
    private AchievementAdapter activeListAdapter;
    private AchievementAdapter completedListAdapter;
    private RecyclerView.LayoutManager uncompletedListLayoutManager;
    private RecyclerView.LayoutManager activeListLayoutManager;
    private RecyclerView.LayoutManager completedListLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievement_scherm);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("AppPref", MODE_PRIVATE);
        final ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        final ApiService service = RetrofitBuilder.createService(ApiService.class);

        db = Room
                .databaseBuilder(getApplicationContext(), AppDatabase.class, "database")
                .build();

        final Thread getUserData = new Thread(){
            @Override
            public void run(){
                User u = db.userDao().loadUserByEmail(pref.getString("loggedInUser", ""));
                xp = u.getXp();
                uncompletedIds = u.getUncompleted();
                activeIds = u.getActive();
                completedIds = u.getCompleted();
                initializeRecyclerview(pref);
                return;
            }
        };

        // checken of device verbonden is met internet
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
        {
            //device is verbonden met internet
            Call<UserInformation> getUserInformationCall = service.getuserdata( "Bearer " + pref.getString("bearer", ""));
            getUserInformationCall.enqueue(new Callback<UserInformation>() {
                @Override
                public void onResponse(Call<UserInformation>call, final Response<UserInformation> response) {
                    // check of lokaal in sync is met api
                    Thread syncUserData = new Thread(){
                        @Override
                        public void run(){
                            final User u = db.userDao().loadUserByEmail(pref.getString("loggedInUser", ""));
                            if(u.getXp() != response.body().xp || !u.getUncompleted().matches(response.body().uncompleted) || !u.getActive().matches(response.body().active) ||
                                    !u.getCompleted().matches(response.body().completed)){
                                // data is niet in sync
                                // hier wordt lokale data verstuurd naar de api om deze weer te synchroniseren
                                UserStatistics userStatistics = new UserStatistics(u.getActive(), u.getCompleted(), u.getUncompleted(), u.getXp());
                                Call<UserStatistics> updateUserInformationCall = service.userdata(userStatistics, "Bearer " + pref.getString("bearer", ""));
                                updateUserInformationCall.enqueue(new Callback<UserStatistics>() {
                                    @Override
                                    public void onResponse(Call<UserStatistics>call, Response<UserStatistics> response) {
                                        Log.d("sync", "Data is synced");
                                        xp = u.getXp();
                                        uncompletedIds = u.getUncompleted();
                                        activeIds = u.getActive();
                                        completedIds = u.getCompleted();
                                        initializeRecyclerview(pref);
                                    }

                                    @Override
                                    public void onFailure(Call<UserStatistics> call, Throwable t) {
                                        Log.d("fout", t.toString());
                                    }
                                });
                            } else {
                                // data loopt synchroon
                                xp = response.body().xp;
                                uncompletedIds = response.body().uncompleted;
                                activeIds = response.body().active;
                                completedIds = response.body().completed;
                                initializeRecyclerview(pref);
                            }
                            return;
                        }
                    };
                    syncUserData.start();
                }

                @Override
                public void onFailure(Call<UserInformation> call, Throwable t) {
                    getUserData.start();
                }
            });
        }else {
            // device is niet verbonden met internet
            getUserData.start();
        }

        Button backButton = findViewById(R.id.achievementSchermBackButton);
        final TextView uncompletedLink = findViewById(R.id.achievementSchermUncompletedLink);
        final TextView completedLink = findViewById(R.id.achievementSchermCompletedLink);
        final TextView activeLink = findViewById(R.id.achievementSchermActiveLink);
        final Drawable linkBackground = ResourcesCompat.getDrawable(getResources(), R.drawable.achievement_scherm_link_background, null);
//        final ScrollView scrollView = findViewById(R.id.achievementSchermScrollView);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.close();
                Intent hoofdscherm = new Intent(getApplicationContext(), HoofdschermActivity.class);
                startActivity(hoofdscherm);
                finish();
            }
        });

        uncompletedLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uncompletedLink.setBackground(linkBackground);
                uncompletedList.setVisibility(View.VISIBLE);
                completedLink.setBackground(null);
                completedList.setVisibility(View.GONE);
                activeLink.setBackground(null);
                activeList.setVisibility(View.GONE);
//                scrollView.smoothScrollTo(0,0);
            }
        });

        completedLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completedLink.setBackground(linkBackground);
                completedList.setVisibility(View.VISIBLE);
                uncompletedLink.setBackground(null);
                uncompletedList.setVisibility(View.GONE);
                activeLink.setBackground(null);
                activeList.setVisibility(View.GONE);
//                scrollView.smoothScrollTo(0,0);
            }
        });

        activeLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeLink.setBackground(linkBackground);
                activeList.setVisibility(View.VISIBLE);
                uncompletedLink.setBackground(null);
                uncompletedList.setVisibility(View.GONE);
                completedLink.setBackground(null);
                completedList.setVisibility(View.GONE);
//                scrollView.smoothScrollTo(0,0);
            }
        });
    }

    // hier wordt het scherm opgebouwd
    public void initializeRecyclerview(final SharedPreferences pref){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createAllAchievementList();
                createRecyclerViewLists();
                fillRecyclerViewLists();
                buildRecyclerViews(pref);
                setCurrentLists();
            }
        });
    }

    // Deze functie zet de string waarde van id's uit de backend om in een int array
    public int[] stringToIntArray(String ids){
        String[] stringArray = ids.split(",", 0);
        int size = stringArray.length;
        int[] idArray = new int[size];
        for(int i = 0; i < size; i++){
            if(!stringArray[i].isEmpty()){
                idArray[i] = Integer.parseInt(stringArray[i]);
            }
        }
        return idArray;
    }

    // Deze functie zet een int array om in een string om te versturen naar de backend
    public String intArrayToString(ArrayList<Achievement> intArray){
        String ids = "";
        for(int i = 0; i < intArray.size(); i++){
            if(i == intArray.size() -1){
                String id = Integer.toString(intArray.get(i).getId());
                ids += id;
            } else {
                String id = Integer.toString(intArray.get(i).getId()) + ",";
                ids += id;
            }
        }
        return ids;
    }

    // hier worden alle achievements geinitialiseerd
    public void createAllAchievementList(){
        this.allAchievements = new ArrayList<>();
        this.allAchievements.add(new Achievement(1, R.drawable.neemplaats, "Neem Plaats", "Ga naast iemand zitten waar je nooit naast zit.", 15));
        this.allAchievements.add(new Achievement(2, R.drawable.wiehebikaandelijn, "Wie heb ik aan de lijn?", "Bel een bekende op en vraag hoe het met diegene gaat.", 20));
        this.allAchievements.add(new Achievement(3, R.drawable.kedengkedeng, "Kedeng kedeng", "Begin een gesprek met een vreemde in de trein.", 30));
        this.allAchievements.add(new Achievement(4, R.drawable.smalltalk, "Small talk", "Ga een willekeurige chatroom in en begin het gesprek.", 10));
        this.allAchievements.add(new Achievement(5, R.drawable.spiegelvandeziel, "Spiegel van de ziel", "Voer een kort gesprekje met iemand, en kijk niet weg. kijk hem/haar aan!", 25));
        this.allAchievements.add(new Achievement(6, R.drawable.hellothere, "Hello there", "Zeg gedag tegen een vreemde", 10));
        this.allAchievements.add(new Achievement(7, R.drawable.iwannarock, "I wanna rock", "Ga naar een karaokebar en zing je favoriete nummer.", 50));
        this.allAchievements.add(new Achievement(8, R.drawable.threeandcounting, "Three and counting!", "Spreek vandaag 3 nieuwe mensen aan.", 30));
        this.allAchievements.add(new Achievement(9, R.drawable.nuchterdenachtdoor, "Nuchter de nacht door", "Ga stappen zonder alcohol te consumeren en ga met mensen praten en dansen.", 45));
        this.allAchievements.add(new Achievement(10, R.drawable.mediumrare, "Medium rare", "Ga lunchen met een collega die je minder goed kent.", 30));
        this.allAchievements.add(new Achievement(11, R.drawable.timetoask, "Time to ask", "Vraag een vreemde de tijd.", 10));
        this.allAchievements.add(new Achievement(12, R.drawable.singingoutloud, "Singing out loud", "Zing luid in de keuken, je kamer en de douche. laat jezelf horen!", 20));
        this.allAchievements.add(new Achievement(13, R.drawable.fiveinarow, "Five in a row", "Spreek vandaag 5 nieuwe mensen aan.", 50));
        this.allAchievements.add(new Achievement(14, R.drawable.callmemaybe, "Call me maybe", "Spreek iemand aan van het andere geslacht en vraag zijn/haar nummer.", 40));
        this.allAchievements.add(new Achievement(15, R.drawable.hotcoffee, "Hot coffee", "Ga met een vreemde koffie drinken.", 45));
        this.allAchievements.add(new Achievement(16, R.drawable.jokesonyou, "Joke's on you", "Maak een grapje.", 15));
        this.allAchievements.add(new Achievement(17, R.drawable.showtime, "Showtime", "Spreek voor een groot publiek.", 50));
        this.allAchievements.add(new Achievement(18, R.drawable.differentme, "Different me", "Trek iets aan wat je nooit aan zou trekken.", 25));
        this.allAchievements.add(new Achievement(19, R.drawable.headsup, "Heads up", "Oefen een goed houding. Hoofd omhoog, schouders naar achteren en borst vooruit.", 15));
        this.allAchievements.add(new Achievement(20, R.drawable.lmfao, "LMFAO", "Glimlach naar vreemde mensen op straat.", 15));
    }

    public void createRecyclerViewLists(){
        // Deze ArrayLists worden uiteindelijk meegegeven aan de RecyclerView
        this.uncompletedAchievements = new ArrayList<>();
        this.activeAchievements = new ArrayList<>();
        this.completedAchievements = new ArrayList<>();
    }

    public void fillRecyclerViewLists(){
        // In onderstaande for loops worden de id's van de backend vergeleken met de totale lijst aan achievements en toegevoegd aan de juiste ArrayList
        for(int id : stringToIntArray(this.uncompletedIds)){
            for(int i = 0; i < this.allAchievements.size(); i++){
                if(id == this.allAchievements.get(i).getId()){
                    this.uncompletedAchievements.add(this.allAchievements.get(i));
                }
            }
        }
        for(int id : stringToIntArray(this.activeIds)){
            for(int i = 0; i < this.allAchievements.size(); i++){
                if(id == this.allAchievements.get(i).getId()){
                    this.activeAchievements.add(this.allAchievements.get(i));
                }
            }
        }
        for(int id : stringToIntArray(this.completedIds)){
            for(int i = 0; i < this.allAchievements.size(); i++){
                if(id == this.allAchievements.get(i).getId()){
                    this.completedAchievements.add(this.allAchievements.get(i));
                }
            }
        }
    }

    public void buildRecyclerViews(final SharedPreferences pref){
        // instanties van layoutmanagers maken voor elke lijst
        this.uncompletedListLayoutManager = new LinearLayoutManager(this);
        this.activeListLayoutManager = new LinearLayoutManager(this);
        this.completedListLayoutManager = new LinearLayoutManager(this);

        // RecyclerViews worden hier gekoppeld aan de juiste view
        this.uncompletedList = findViewById(R.id.uncompleted);
        this.activeList = findViewById(R.id.active);
        this.completedList = findViewById(R.id.completed);

        // instanties van adapters maken voor elke lijst
        this.uncompletedListAdapter = new AchievementAdapter(uncompletedAchievements, "UNCOMPLETED");
        this.activeListAdapter = new AchievementAdapter(activeAchievements, "ACTIVE");
        this.completedListAdapter = new AchievementAdapter(completedAchievements, "COMPLETED");

        // De juiste LayoutManagers worden
        // gekoppeld aan de RecyclerViews
        this.uncompletedList.setLayoutManager(this.uncompletedListLayoutManager);
        this.activeList.setLayoutManager(this.activeListLayoutManager);
        this.completedList.setLayoutManager(this.completedListLayoutManager);

        // De juiste adapters worden gekoppeld aan de RecyclerViews
        this.uncompletedList.setAdapter(this.uncompletedListAdapter);
        this.activeList.setAdapter(this.activeListAdapter);
        this.completedList.setAdapter(this.completedListAdapter);

        uncompletedListAdapter.setOnItemClickListener(new AchievementAdapter.OnItemClickListener() {
            @Override
            public void onStartClick(int position) {
                int id = uncompletedAchievements.get(position).getId();
                for(int i = 0; i < allAchievements.size(); i++){
                    if(allAchievements.get(i).getId() == id){
                        activeAchievements.add(allAchievements.get(i));
                        uncompletedAchievements.remove(position);
                        currentActiveIds = intArrayToString(activeAchievements);
                        currentUncompletedIds = intArrayToString(uncompletedAchievements);
                    }
                }
                uncompletedListAdapter.notifyItemRemoved(position);
                activeListAdapter.notifyItemInserted(activeAchievements.size());

                ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
                {
                    ApiService service = RetrofitBuilder.createService(ApiService.class);
                    UserStatistics userStatistics = new UserStatistics(currentActiveIds, currentCompletedIds, currentUncompletedIds, xp);
                    Call<UserStatistics> updateUserInformationCall = service.userdata(userStatistics, "Bearer " + pref.getString("bearer", ""));
                    updateUserInformationCall.enqueue(new Callback<UserStatistics>() {
                        @Override
                        public void onResponse(Call<UserStatistics>call, Response<UserStatistics> response) {
                            setUserData = new Thread(){
                                @Override
                                public void run(){
                                    db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                                    return;
                                }
                            };
                            setUserData.start();
                        }

                        @Override
                        public void onFailure(Call<UserStatistics> call, Throwable t) {
                            setUserData = new Thread(){
                                @Override
                                public void run(){
                                    db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                                    return;
                                }
                            };
                            setUserData.start();
                        }
                    });
                } else {
                    setUserData = new Thread(){
                        @Override
                        public void run(){
                            db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                            return;
                        }
                    };
                    setUserData.start();
                }
            }

            @Override
            public void onCancelClick(int position) {

            }

            @Override
            public void onFinishClick(int position) {

            }
        });

        activeListAdapter.setOnItemClickListener(new AchievementAdapter.OnItemClickListener() {
            @Override
            public void onStartClick(int position) {

            }

            @Override
            public void onCancelClick(int position) {
                int id = activeAchievements.get(position).getId();
                for(int i = 0; i < allAchievements.size(); i++){
                    if(allAchievements.get(i).getId() == id){
                        uncompletedAchievements.add(allAchievements.get(i));
                        activeAchievements.remove(position);
                        currentUncompletedIds = intArrayToString(uncompletedAchievements);
                        currentActiveIds = intArrayToString(activeAchievements);
                    }
                }
                activeListAdapter.notifyItemRemoved(position);
                uncompletedListAdapter.notifyItemInserted(uncompletedAchievements.size());

                ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
                {
                    ApiService service = RetrofitBuilder.createService(ApiService.class);
                    UserStatistics userStatistics = new UserStatistics(currentActiveIds, currentCompletedIds, currentUncompletedIds, xp);
                    Call<UserStatistics> updateUserInformationCall = service.userdata(userStatistics, "Bearer " + pref.getString("bearer", ""));
                    updateUserInformationCall.enqueue(new Callback<UserStatistics>() {
                        @Override
                        public void onResponse(Call<UserStatistics>call, Response<UserStatistics> response) {
                            setUserData = new Thread(){
                                @Override
                                public void run(){
                                    db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                                    return;
                                }
                            };
                            setUserData.start();
                        }

                        @Override
                        public void onFailure(Call<UserStatistics> call, Throwable t) {
                            setUserData = new Thread(){
                                @Override
                                public void run(){
                                    db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                                    return;
                                }
                            };
                            setUserData.start();
                        }
                    });
                } else {
                    setUserData = new Thread(){
                        @Override
                        public void run(){
                            db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                            return;
                        }
                    };
                    setUserData.start();
                }
            }

            @Override
            public void onFinishClick(int position) {
                int id = activeAchievements.get(position).getId();
                for(int i = 0; i < allAchievements.size(); i++){
                    if(allAchievements.get(i).getId() == id){
                        completedAchievements.add(allAchievements.get(i));
                        activeAchievements.remove(position);
                        currentCompletedIds = intArrayToString(completedAchievements);
                        currentActiveIds = intArrayToString(activeAchievements);
                        xp += allAchievements.get(i).getXp();
                    }
                }
                activeListAdapter.notifyItemRemoved(position);
                completedListAdapter.notifyItemInserted(completedAchievements.size());

                ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
                {
                    ApiService service = RetrofitBuilder.createService(ApiService.class);
                    UserStatistics userStatistics = new UserStatistics(currentActiveIds, currentCompletedIds, currentUncompletedIds, xp);
                    Call<UserStatistics> updateUserInformationCall = service.userdata(userStatistics, "Bearer " + pref.getString("bearer", ""));
                    updateUserInformationCall.enqueue(new Callback<UserStatistics>() {
                        @Override
                        public void onResponse(Call<UserStatistics>call, Response<UserStatistics> response) {
                            setUserData = new Thread(){
                                @Override
                                public void run(){
                                    db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                                    return;
                                }
                            };
                            setUserData.start();
                        }

                        @Override
                        public void onFailure(Call<UserStatistics> call, Throwable t) {
                            setUserData = new Thread(){
                                @Override
                                public void run(){
                                    db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                                    return;
                                }
                            };
                            setUserData.start();
                        }
                    });
                } else {
                    setUserData = new Thread(){
                        @Override
                        public void run(){
                            db.userDao().updateUser(xp, currentActiveIds, currentCompletedIds, currentUncompletedIds, pref.getString("loggedInUser", ""));
                            return;
                        }
                    };
                    setUserData.start();
                }

            }
        });
    }

    public void setCurrentLists(){
        this.currentActiveIds = intArrayToString(activeAchievements);
        this.currentCompletedIds = intArrayToString(completedAchievements);
        this.currentUncompletedIds = intArrayToString(uncompletedAchievements);
    }
}
