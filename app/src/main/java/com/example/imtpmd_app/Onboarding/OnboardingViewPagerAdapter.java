package com.example.imtpmd_app.Onboarding;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.imtpmd_app.R;

import java.util.List;

public class OnboardingViewPagerAdapter extends PagerAdapter {

    Context context;
    List<ScreenItem> listScreen;

    public OnboardingViewPagerAdapter(Context context, List<ScreenItem> listScreen) {
        this.context = context;
        this.listScreen = listScreen;
    }

    @Override
    public int getCount() {
        return listScreen.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater)  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutOnboarding = inflater.inflate(R.layout.layout_onboarding_scherm,null);

        ImageView onboardingImage = layoutOnboarding.findViewById(R.id.onboardingImage);
        TextView onboardingTitle = layoutOnboarding.findViewById(R.id.onboardingTitle);
        TextView onboardingDescription = layoutOnboarding.findViewById(R.id.onboardingDescription);

        onboardingTitle.setText(listScreen.get(position).getTitle());
        onboardingDescription.setText(listScreen.get(position).getDescription());
        onboardingImage.setImageResource(listScreen.get(position).getScreenImage());

        container.addView(layoutOnboarding);

        return layoutOnboarding;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
