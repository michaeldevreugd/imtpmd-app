package com.example.imtpmd_app;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    //Deze variabelen gebruiken we om de input uit de velden te halen
    private EditText usernameInput;
    private EditText emailInput;
    private EditText passwordInput;

    //Apiservice maakt service aan om een request uit te kunnen voeren
    ApiService service;
    //Hiermee maken we een call waarmee we informatie van de AccessToken ophalen
    Call<AccessToken> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Deze variabelen gebruiken we om de input uit de velden te halen
        usernameInput = (EditText)findViewById(R.id.usernameInput);
        emailInput = (EditText)findViewById(R.id.emailRegisterInput);
        passwordInput = (EditText)findViewById(R.id.passwordRegisterInput);

        //Hier maken we onze service aan met de bijhorende klasse
        service = RetrofitBuilder.createService(ApiService.class);
        //Deze variabele kijkt of je verbinding nog bestaat of verloren is
        final ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("AppPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();

        final AppDatabase db = Room
                .databaseBuilder(getApplicationContext(), AppDatabase.class, "database")
                .fallbackToDestructiveMigration()
                .build();

        final Thread dataThread = new Thread(){
            @Override
            public void run(){
                String username = usernameInput.getText().toString();
                String email = emailInput.getText().toString();

                if(!username.matches("") && !email.matches("")){
                    // aanmaken van de nieuwe user
                    User u = new User();
                    u.setEmail(email.trim());
                    u.setUsername(username);
                    u.setXp(0);
                    u.setActive("");
                    u.setCompleted("");
                    u.setUncompleted("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20");
                    db.userDao().insertall(u);

                    // sharedpref 'registered' op true zetten zodat de startschermactivity
                    // kan zien dat er al een keer geregistreerd is en gelijk kan verwijzen naar het login/hoofdscherm
                    editor.putBoolean("registered", true);
                    editor.commit();
                    db.close();
                    return;
                }
            }
        };

        Button signUpButton = (Button)findViewById(R.id.signUpButton);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username2 = usernameInput.getText().toString();
                String email2 = emailInput.getText().toString();
                String password = passwordInput.getText().toString();

                usernameInput.setError(null);
                emailInput.setError(null);
                passwordInput.setError(null);

                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                    call = service.register(username2,email2.trim(),password);
                    call.enqueue(new Callback<AccessToken>() {
                        @Override
                        public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {

                            if(response.isSuccessful())
                            {
                                dataThread.start();
                                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                                finish();

                            }else
                            {
                                handleErrors(response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Call<AccessToken> call, Throwable t) {
                            Toast.makeText(RegisterActivity.this, "Oeps, er is iets fout gegaan.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else{
                    Toast.makeText(RegisterActivity.this, "U bent niet verbonden met internet", Toast.LENGTH_SHORT).show();
                }
            }
        });

        TextView loginLink = findViewById(R.id.registerLoginLink);

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginScherm = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginScherm);
                finish();
            }
        });
    }

    private void handleErrors(ResponseBody response)
    {
        ApiError apiError = Utils.convertErrors(response);

        for(Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {
            if(error.getKey().equals("name"))
            {
                usernameInput.setError(error.getValue().get(0));
            }

            if(error.getKey().equals("email"))
            {
                emailInput.setError(error.getValue().get(0));
            }
            if(error.getKey().equals("password"))
            {
                passwordInput.setError(error.getValue().get(0));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(call != null) {
            call.cancel();
            call = null;
        }
    }
}
