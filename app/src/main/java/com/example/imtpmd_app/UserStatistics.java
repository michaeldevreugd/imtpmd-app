package com.example.imtpmd_app;

public class UserStatistics {
    public String active;
    public String completed;
    public String uncompleted;
    public int xp;

    public UserStatistics(String active, String completed, String uncompleted, int xp) {
        this.active = active;
        this.completed = completed;
        this.uncompleted = uncompleted;
        this.xp = xp;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getUncompleted() {
        return uncompleted;
    }

    public void setUncompleted(String uncompleted) {
        this.uncompleted = uncompleted;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }
}
