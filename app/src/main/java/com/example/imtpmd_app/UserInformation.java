package com.example.imtpmd_app;

import com.squareup.moshi.Json;

public class UserInformation {

    @Json(name = "active")
    String active;
    @Json(name = "uncompleted")
    String uncompleted;
    @Json(name = "completed")
    String completed;
    @Json(name = "xp")
    int xp;
    @Json(name = "name")
    String name;

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUncompleted() {
        return uncompleted;
    }

    public void setUncompleted(String uncompleted) {
        this.uncompleted = uncompleted;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
