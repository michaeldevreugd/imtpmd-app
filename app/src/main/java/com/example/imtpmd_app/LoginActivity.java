package com.example.imtpmd_app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private EditText emailInput;
    private EditText passwordInput;
    ApiService service;
    Call<AccessToken> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences("AppPref", MODE_PRIVATE);
        final SharedPreferences.Editor editor = pref.edit();
        final ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        service = RetrofitBuilder.createService(ApiService.class);

        emailInput = (EditText)findViewById(R.id.emailRegisterInput);
        passwordInput = (EditText)findViewById(R.id.passwordRegisterInput);

        TextView registerLink = findViewById(R.id.loginRegisterLink);
        Button loginButton = findViewById(R.id.loginButton);

        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerScherm = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(registerScherm);
                finish();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = emailInput.getText().toString();
                String password = passwordInput.getText().toString();

                emailInput.setError(null);
                passwordInput.setError(null);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                    call = service.login(email.trim(),password);
                    call.enqueue(new Callback<AccessToken>() {
                        @Override
                        public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {

                            Log.w(TAG, "onResponse: " + response);

                            if(response.isSuccessful())
                            {
                                editor.putString("loggedInUser", email.trim());
                                editor.putString("bearer", response.body().accessToken);
                                editor.commit();
                                startActivity(new Intent(LoginActivity.this,HoofdschermActivity.class));
                                finish();
                            }else {
                                if(response.code() == 422)
                                {
                                    handleErrors(response.errorBody());
                                }

                                if(response.code() == 401)
                                {
                                    ApiError apiError = Utils.convertErrors(response.errorBody());
                                    Toast.makeText(LoginActivity.this, apiError.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<AccessToken> call, Throwable t) {
                            Log.w(TAG, "onFailure: " + t.getMessage());
                        }
                    });
                }else{
                    Toast.makeText(LoginActivity.this, "U bent niet verbonden met internet", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void handleErrors(ResponseBody response)
    {
        ApiError apiError = Utils.convertErrors(response);

        for(Map.Entry<String, List<String>> error : apiError.getErrors().entrySet()) {

            if(error.getKey().equals("username"))
            {
                emailInput.setError(error.getValue().get(0));
            }
            if(error.getKey().equals("password"))
            {
                passwordInput.setError(error.getValue().get(0));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(call != null) {
            call.cancel();
            call = null;
        }
    }
}
