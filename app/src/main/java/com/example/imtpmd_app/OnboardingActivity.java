package com.example.imtpmd_app;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.imtpmd_app.Onboarding.OnboardingViewPagerAdapter;
import com.example.imtpmd_app.Onboarding.ScreenItem;

import java.util.ArrayList;
import java.util.List;

public class OnboardingActivity extends AppCompatActivity {


    private ViewPager viewPager;
    OnboardingViewPagerAdapter onboardingViewPagerAdapter;
    TabLayout tabIndicator;
    Button buttonNext;
    Button buttonGetStarted;
    int position =  0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        tabIndicator = findViewById(R.id.tabIndicator);
        buttonNext = findViewById(R.id.buttonNext);
        buttonGetStarted = findViewById(R.id.buttonGetStarted);

        final List<ScreenItem> list = new ArrayList<>();
        list.add(new ScreenItem("Face your fears","Verhelp je anxiety met kleine stapjes",R.drawable.onboardingeen));
        list.add(new ScreenItem("Find discomfort","Daag jezelf uit en doe challenges",R.drawable.onboardingdrie));
        list.add(new ScreenItem("Be a hero","Help andere ook uit hun schulp te komen",R.drawable.onboardingtwee));
        viewPager = findViewById(R.id.onboardingViewPager);
        onboardingViewPagerAdapter = new OnboardingViewPagerAdapter(this,list);
        viewPager.setAdapter(onboardingViewPagerAdapter);

        tabIndicator.setupWithViewPager(viewPager);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position = viewPager.getCurrentItem();
                if(position < list.size())
                {
                    position++;
                    viewPager.setCurrentItem(position);
                }

                if(position == list.size())
                {
                    // TODO : Show the "Register" button to the screen
                    loadLastScreen();
                }
            }
        });

        buttonGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerscherm = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(registerscherm);
                finish();
            }
        });
    }

    private void loadLastScreen() {
        buttonGetStarted.setVisibility(View.VISIBLE);
        tabIndicator.setVisibility(View.INVISIBLE);
        buttonNext.setVisibility(View.INVISIBLE);
    }
}
