package com.example.imtpmd_app;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user")
    List<User> loadAll();
    @Query("SELECT * FROM user WHERE email IN (:userEmails)")
    User loadUserByEmail(String... userEmails);
    @Query("UPDATE User SET xp=:xp, active=:active, completed=:completed, uncompleted=:uncompleted WHERE email = :email")
    void updateUser(int xp, String active, String completed, String uncompleted, String email);
    @Insert
    void insertall(User... users);
    @Delete
    void delete(User user);
}
