package com.example.imtpmd_app;

public class Achievement {
    private int id;
    private int imageResource;
    private String title;
    private String description;
    private int xp;

    public Achievement(int id, int imageResource, String title, String description, int xp){
        this.id = id;
        this.imageResource = imageResource;
        this.title = title;
        this.description = description;
        this.xp = xp;
    }

    public int getId(){
        return this.id;
    }

    public int getImageResource(){
        return this.imageResource;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getXp() {
        return xp;
    }
}
